package by.itacademy.kursproject.workshop;

import by.itacademy.kursproject.workshop.menu.root.LocaleChoiceMenuItem;
import by.itacademy.kursproject.workshop.menu.root.RootMenuItem;

public class Main {
    public static void main(String[] args) {
        RootMenuItem rootMenu = new LocaleChoiceMenuItem();
        rootMenu.execute();
    }
}
