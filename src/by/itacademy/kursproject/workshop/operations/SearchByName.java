package by.itacademy.kursproject.workshop.operations;

import by.itacademy.kursproject.workshop.service.Service;
import by.itacademy.kursproject.workshop.service.ServiceList;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;

public class SearchByName implements Operation {
    private Map<String, Service> serviceMap = new HashMap<>();
    private static final Scanner SCANNER = new Scanner(System.in);

    @Override
    public String execute(ServiceList serviceList) throws NullPointerException {
        for (Service i : serviceList.getServices()) {
            serviceMap.put(i.getName(), i);
        }
        String key = SCANNER.nextLine();
        if (serviceMap.get(key) == null) {
            throw new NullPointerException(ResourceBundle.getBundle("data").getString("ServiceNotFound"));
        } else {
            return serviceMap.get(key).toString();
        }
    }
}
