package by.itacademy.kursproject.workshop.operations;

import by.itacademy.kursproject.workshop.service.Service;
import by.itacademy.kursproject.workshop.service.ServiceList;

public class AdditionService implements Operation {

    private Service service;

    @Override
    public String execute(ServiceList serviceList) {
        serviceList.addService(service);
        return "";
    }

    public AdditionService(Service service) {
        this.service = service;
    }
}
