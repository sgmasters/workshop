package by.itacademy.kursproject.workshop.operations;

import by.itacademy.kursproject.workshop.comparators.ById;
import by.itacademy.kursproject.workshop.menu.menuoperation.SortService;

import java.util.Comparator;

public class SortById extends SortService {

    @Override
    protected Comparator getComparator() {
        return new ById();
    }

}
