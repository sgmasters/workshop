package by.itacademy.kursproject.workshop.operations;

import by.itacademy.kursproject.workshop.service.Service;
import by.itacademy.kursproject.workshop.service.ServiceList;

public class RemoveService implements Operation {
    private Service service;

    @Override
    public String execute(ServiceList serviceList) {
        serviceList.removeService(service);
        return "";
    }

    public RemoveService(Service service) {
        this.service = service;
    }
}
