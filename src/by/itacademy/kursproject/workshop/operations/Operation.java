package by.itacademy.kursproject.workshop.operations;

import by.itacademy.kursproject.workshop.service.ServiceList;

public interface Operation {
    String execute(ServiceList serviceList) throws NullPointerException;
}
