package by.itacademy.kursproject.workshop.operations;

import by.itacademy.kursproject.workshop.service.MaterialCosts;
import by.itacademy.kursproject.workshop.service.Service;
import by.itacademy.kursproject.workshop.service.ServiceList;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PercentOfMaterialCosts implements Operation {
    private List<MaterialCosts> materialCosts = Arrays.asList(MaterialCosts.values());
    private ServiceList serviceList;
    private Map<MaterialCosts, Integer> serviceMap = new HashMap<>();

    @Override
    public String execute(ServiceList serviceList) {
        this.serviceList = serviceList;
        StringBuilder s = new StringBuilder();
        for (MaterialCosts i : materialCosts) {
            s.append(i.name()).append(":\n").append(getPercent(i)).append("\n");
        }
        return s.toString();
    }

    private int getPercent(MaterialCosts materialCost) {
        for (Service j : serviceList.getServices()) {
            if (materialCost.equals(j.getMaterialCosts())) {
                serviceMap.putIfAbsent(materialCost, 0);
                serviceMap.put(materialCost, serviceMap.get(materialCost) + 1);
            }
        }
        serviceMap.putIfAbsent(materialCost, 0);
        return 100 * serviceMap.get(materialCost) / serviceList.getServices().size();
    }

}
