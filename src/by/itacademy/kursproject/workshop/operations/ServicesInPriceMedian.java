package by.itacademy.kursproject.workshop.operations;

import by.itacademy.kursproject.workshop.comparators.ByPriceThenId;
import by.itacademy.kursproject.workshop.service.Service;
import by.itacademy.kursproject.workshop.service.ServiceList;

import java.util.List;
import java.util.ResourceBundle;

public class ServicesInPriceMedian implements Operation {

    @Override
    public String execute(ServiceList serviceList) {
        ResourceBundle bundle = ResourceBundle.getBundle("data");
        int medianPrice = getMedian(serviceList.getServices());
        StringBuilder s = new StringBuilder(bundle.getString("medianPrice") + " = ");
        s.append(medianPrice).append("\n").append(bundle.getString("ServicesInMedianPrice")).append(":");
        for (Service service : serviceList.getServices()) {
            if (service.getPrice() == medianPrice) {
                s.append(service);
            }
        }
        return s.toString();
    }

    private int getMedian(List<Service> list) {
        list.sort(new ByPriceThenId());
        int N = list.size();
        int medianPrice;
        if (N % 2 != 0) {
            medianPrice = list.get((N + 1) / 2).getPrice();
        } else {
            medianPrice = (list.get(N / 2).getPrice() + list.get((N / 2) + 1).getPrice()) / 2;
        }
        return medianPrice;
    }


}
