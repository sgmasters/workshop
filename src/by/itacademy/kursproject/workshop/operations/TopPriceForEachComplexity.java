package by.itacademy.kursproject.workshop.operations;

import by.itacademy.kursproject.workshop.comparators.ByPriceThenId;
import by.itacademy.kursproject.workshop.service.Complexity;
import by.itacademy.kursproject.workshop.service.Service;
import by.itacademy.kursproject.workshop.service.ServiceList;

import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.TreeSet;

public class TopPriceForEachComplexity implements Operation {
    private List<Complexity> complexities = Arrays.asList(Complexity.values());
    private List<Service> serviceList;
    private StringBuilder s = new StringBuilder();

    @Override
    public String execute(ServiceList serviceList) {
        this.serviceList = serviceList.getServices();
        for (Complexity i : complexities) {
            s.append(ResourceBundle.getBundle("data").getString("TopPriceOfComplexity")).append(" ").append(i.name()).append(":\n");
            getTopPrice(i);
            s.append("\n");
        }
        return s.toString();
    }

    private void getTopPrice(Complexity complexity) {
        TreeSet<Service> serviceTreeSet = new TreeSet<>(new ByPriceThenId());
        for (Service i : serviceList) {
            if (i.getComplexity().equals(complexity)) {
                serviceTreeSet.add(i);
            }
        }
        for (Service i : serviceTreeSet) {
            if (i.getPrice() == serviceTreeSet.last().getPrice()) {
                s.append(i);
            }
        }
    }


}
