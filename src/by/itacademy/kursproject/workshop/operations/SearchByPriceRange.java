package by.itacademy.kursproject.workshop.operations;

import by.itacademy.kursproject.workshop.service.Service;
import by.itacademy.kursproject.workshop.service.ServiceList;

import java.util.HashMap;
import java.util.Map;

public class SearchByPriceRange implements Operation {
    private Map<Integer, Service> serviceMap = new HashMap<>();
    private int min;
    private int max;

    public SearchByPriceRange(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public String execute(ServiceList serviceList) {
        for (Service i : serviceList.getServices()) {
            serviceMap.put(i.getPrice(), i);
        }
        StringBuilder s = new StringBuilder();
        for (Integer i : serviceMap.keySet()) {
            if (i <= max && i >= min) {
                s.append(serviceMap.get(i));
                s.append("\n");
            }
        }
        return s.toString();
    }
}
