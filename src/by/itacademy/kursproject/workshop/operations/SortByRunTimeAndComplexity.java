package by.itacademy.kursproject.workshop.operations;

import by.itacademy.kursproject.workshop.comparators.ByRunTimeAndComplexity;
import by.itacademy.kursproject.workshop.menu.menuoperation.SortService;

import java.util.Comparator;

public class SortByRunTimeAndComplexity extends SortService {

    @Override
    protected Comparator getComparator() {
        return new ByRunTimeAndComplexity();
    }

}
