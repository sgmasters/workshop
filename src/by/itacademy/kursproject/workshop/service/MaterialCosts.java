package by.itacademy.kursproject.workshop.service;

public enum MaterialCosts {
    NOTHING,
    LOW,
    AVERAGE,
    HIGH
}
