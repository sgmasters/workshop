package by.itacademy.kursproject.workshop.service;

public class Service {
    private String name;
    private double runTime;
    private int price;
    private Complexity complexity;
    private MaterialCosts materialCosts;
    private int id;

    public Service(String name, double runTime, int price, Complexity complexity, MaterialCosts materialCosts, int id) {
        this.name = name;
        this.runTime = runTime;
        this.price = price;
        this.complexity = complexity;
        this.materialCosts = materialCosts;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("\n");
        s.append("id=" + id +
                ", \nname='" + name + '\'' +
                ", \nrunTime=" + runTime +
                ", \nprice=" + price +
                ", \ncomplexity=" + complexity +
                ", \nmaterialCosts=" + materialCosts +
                "\n");
        return s.toString();
    }

    public int getPrice() {
        return price;
    }

    public Complexity getComplexity() {
        return complexity;
    }

    public MaterialCosts getMaterialCosts() {
        return materialCosts;
    }

    public double getRunTime() {
        return runTime;
    }

    public int getId() {
        return id;
    }
}
