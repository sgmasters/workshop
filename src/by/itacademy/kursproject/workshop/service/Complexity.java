package by.itacademy.kursproject.workshop.service;

public enum Complexity {
    SIMPLE,
    MEDIUM,
    HARD
}
