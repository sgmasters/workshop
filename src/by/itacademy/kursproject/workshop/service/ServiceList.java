package by.itacademy.kursproject.workshop.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ServiceList {
    private ArrayList<Service> services = new ArrayList<>(Arrays.asList(
            new Service("Creation 2D layout by template", 0.5, 20, Complexity.SIMPLE, MaterialCosts.NOTHING, 1),
            new Service("Workshop rent", 8, 1000, Complexity.MEDIUM, MaterialCosts.AVERAGE, 2),
            new Service("Engraving", 1, 1000, Complexity.HARD, MaterialCosts.LOW, 3),
            new Service("Creation 3D layout", 12, 500, Complexity.HARD, MaterialCosts.NOTHING, 4),
            new Service("Manufacturing metallic details", 6, 200, Complexity.HARD, MaterialCosts.HIGH, 5),
            new Service("Repair light box", 15, 1200, Complexity.HARD, MaterialCosts.AVERAGE, 6)
    ));

    public void addService(Service service) {
        this.services.add(service);
    }

    public void removeService(Service service) {
        this.services.remove(service);
    }

    public List<Service> getServices() {
        return services;
    }
}
