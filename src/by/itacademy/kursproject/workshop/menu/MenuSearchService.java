package by.itacademy.kursproject.workshop.menu;


import by.itacademy.kursproject.workshop.menu.menuoperation.MenuOperation;
import by.itacademy.kursproject.workshop.menu.menuoperation.MenuSearchByName;
import by.itacademy.kursproject.workshop.menu.menuoperation.MenuSearchByPriceRange;
import by.itacademy.kursproject.workshop.menu.root.RootMenuItem;

import java.util.ResourceBundle;

public class MenuSearchService extends MenuCommonSubMenu implements Menu {
    private static final MenuOperation[] subMenus = {
            new MenuSearchByName(),
            new MenuSearchByPriceRange()
    };

    public MenuSearchService(RootMenuItem rootMenuItem) {
        super(subMenus, rootMenuItem);
    }

    @Override
    public String name() {
        return ResourceBundle.getBundle("data").getString("searchService");
    }
}
