package by.itacademy.kursproject.workshop.menu.root;

import java.util.Locale;

public interface RootMenuItem {
    void execute();

    Locale getLocale();
}
