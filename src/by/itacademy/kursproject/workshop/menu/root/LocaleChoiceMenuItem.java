package by.itacademy.kursproject.workshop.menu.root;

import by.itacademy.kursproject.workshop.locale.Locales;

import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LocaleChoiceMenuItem implements RootMenuItem {
    private Locales locales = new Locales();
    private Scanner scanner = new Scanner(System.in);
    private RootMenu next = new RootMenu(this);
    private Locale localeChoice;
    private static final Logger LOGGER = Logger.getLogger(LocaleChoiceMenuItem.class.getName());

    @Override
    public void execute() {
        StringBuilder show = new StringBuilder("Choose locale:\n");
        for (int i = 0; i < locales.size(); i++) {
            show.append(i + 1).append(". ").append(locales.get(i)).append('\n');
        }
        System.out.print(show);
        try {
            int choice = Integer.parseInt(scanner.next());
            localeChoice = locales.get(choice - 1);
        } catch (NumberFormatException | IndexOutOfBoundsException | InputMismatchException e) {
            System.err.println("Incorrect Menu Element");
            LOGGER.log(Level.INFO, e.getMessage());
            this.execute();
        }
        Locale.setDefault(localeChoice);
        next.execute();
    }

    public Locale getLocale() {
        return localeChoice;
    }
}
