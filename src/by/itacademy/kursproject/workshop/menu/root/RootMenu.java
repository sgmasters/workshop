package by.itacademy.kursproject.workshop.menu.root;

import by.itacademy.kursproject.workshop.menu.Menu;
import by.itacademy.kursproject.workshop.menu.MenuExit;
import by.itacademy.kursproject.workshop.menu.MenuManageService;
import by.itacademy.kursproject.workshop.menu.MenuSearchService;
import by.itacademy.kursproject.workshop.menu.MenuSortService;
import by.itacademy.kursproject.workshop.menu.MenuStatistic;
import by.itacademy.kursproject.workshop.service.ServiceList;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RootMenu implements RootMenuItem {

    private RootMenuItem rootMenuItem;
    private static final Scanner SCANNER = new Scanner(System.in);
    private Menu[] menus = {
            new MenuManageService(this),
            new MenuSearchService(this),
            new MenuSortService(this),
            new MenuStatistic(this),
            new MenuExit()
    };
    protected ServiceList serviceList = new ServiceList();
    private static final Logger LOGGER = Logger.getLogger(RootMenu.class.getName());

    public RootMenu(RootMenuItem rootMenuItem) {
        this.rootMenuItem = rootMenuItem;
    }

    public void execute() {
        rootMenuItem.getLocale();
        StringBuilder s = new StringBuilder(getBundle().getString("chooseOperation") + ":\n");
        for (int i = 0; i < menus.length; i++) {
            s.append(i + 1).append(" ").append(menus[i].name()).append("\n");
        }
        System.out.print(s);
        try {
            menus[Integer.valueOf(SCANNER.next()) - 1].execute(serviceList);
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.err.println("Incorrect Menu Element");
            LOGGER.log(Level.INFO, e.getMessage());
            this.execute();
        }
    }

    public ResourceBundle getBundle() {
        return ResourceBundle.getBundle("data");
    }

    @Override
    public Locale getLocale() {
        return rootMenuItem.getLocale();
    }
}
