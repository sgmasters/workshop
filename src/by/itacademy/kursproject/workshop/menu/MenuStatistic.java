package by.itacademy.kursproject.workshop.menu;

import by.itacademy.kursproject.workshop.menu.menuoperation.MenuOperation;
import by.itacademy.kursproject.workshop.menu.menuoperation.MenuPercentForEachMaterialCost;
import by.itacademy.kursproject.workshop.menu.menuoperation.MenuServicesInPriceMedian;
import by.itacademy.kursproject.workshop.menu.menuoperation.MenuTopPriceForComplexity;
import by.itacademy.kursproject.workshop.menu.root.RootMenuItem;

import java.util.ResourceBundle;

public class MenuStatistic extends MenuCommonSubMenu {
    private static final MenuOperation[] subMenus = {
            new MenuPercentForEachMaterialCost(),
            new MenuServicesInPriceMedian(),
            new MenuTopPriceForComplexity()
    };

    public MenuStatistic(RootMenuItem rootMenuItem) {
        super(subMenus, rootMenuItem);
    }

    @Override
    public String name() {
        return ResourceBundle.getBundle("data").getString("Statistic");
    }
}
