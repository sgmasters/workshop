package by.itacademy.kursproject.workshop.menu;

import by.itacademy.kursproject.workshop.service.ServiceList;

public interface Menu {
    void execute(ServiceList serviceList);
    String name();
}
