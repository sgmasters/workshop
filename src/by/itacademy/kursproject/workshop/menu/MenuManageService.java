package by.itacademy.kursproject.workshop.menu;

import by.itacademy.kursproject.workshop.menu.menuoperation.MenuAdditionService;
import by.itacademy.kursproject.workshop.menu.menuoperation.MenuOperation;
import by.itacademy.kursproject.workshop.menu.menuoperation.MenuRemoveService;
import by.itacademy.kursproject.workshop.menu.root.RootMenuItem;

import java.util.ResourceBundle;

public class MenuManageService extends MenuCommonSubMenu implements Menu {
    private static final MenuOperation[] subMenus = {
            new MenuAdditionService(),
            new MenuRemoveService()
    };

    public MenuManageService(RootMenuItem rootMenuItem) {
        super(subMenus, rootMenuItem);
    }

    @Override
    public String name() {
        return ResourceBundle.getBundle("data").getString("manageService");
    }
}
