package by.itacademy.kursproject.workshop.menu.menuoperation;

import by.itacademy.kursproject.workshop.operations.SearchByPriceRange;
import by.itacademy.kursproject.workshop.service.ServiceList;

import java.util.ResourceBundle;
import java.util.Scanner;

public class MenuSearchByPriceRange implements MenuOperation {
    private static final Scanner SCANNER = new Scanner(System.in);

    @Override
    public void execute(ServiceList serviceList) {
        System.out.println(ResourceBundle.getBundle("data").getString("InputMinPrice") + ":");
        int min = Integer.parseInt(SCANNER.next());
        System.out.println(ResourceBundle.getBundle("data").getString("InputMaxPrice") + ":");
        int max = Integer.parseInt(SCANNER.next());
        System.out.println(new SearchByPriceRange(min, max).execute(serviceList));
    }

    @Override
    public String name() {
        return ResourceBundle.getBundle("data").getString("SearchByPriceRange");
    }
}
