package by.itacademy.kursproject.workshop.menu.menuoperation;

import by.itacademy.kursproject.workshop.operations.TopPriceForEachComplexity;
import by.itacademy.kursproject.workshop.service.ServiceList;

import java.util.ResourceBundle;

public class MenuTopPriceForComplexity implements MenuOperation {

    @Override
    public void execute(ServiceList serviceList) {
        System.out.println(new TopPriceForEachComplexity().execute(serviceList));
    }

    @Override
    public String name() {
        return ResourceBundle.getBundle("data").getString("MenuTopPriceForEachComplexity");
    }
}
