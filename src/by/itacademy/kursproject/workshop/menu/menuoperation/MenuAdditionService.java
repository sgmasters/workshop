package by.itacademy.kursproject.workshop.menu.menuoperation;

import by.itacademy.kursproject.workshop.operations.AdditionService;
import by.itacademy.kursproject.workshop.service.ServiceList;

import java.util.ResourceBundle;

public class MenuAdditionService extends ManageService {

    private int id;

    public void execute(ServiceList serviceList) {
        id = serviceList.getServices().size() + 1;
        new AdditionService(getServiceData()).execute(serviceList);
    }

    @Override
    public String name() {
        return ResourceBundle.getBundle("data").getString("MenuAdditionService");
    }

    @Override
    public int getId() {
        return id;
    }
}
