package by.itacademy.kursproject.workshop.menu.menuoperation;

import by.itacademy.kursproject.workshop.service.ServiceList;

import java.util.Collections;
import java.util.Comparator;
import java.util.ResourceBundle;

public abstract class SortService implements MenuOperation {

    public void execute(ServiceList serviceList) {
        Collections.sort(serviceList.getServices(), getComparator());
        System.out.println(serviceList.getServices());
    }

    protected abstract Comparator getComparator();

    @Override
    public String name() {
        return ResourceBundle.getBundle("data").getString("sortServices");
    }
}
