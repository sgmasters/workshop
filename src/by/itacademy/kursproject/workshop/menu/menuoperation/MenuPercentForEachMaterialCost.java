package by.itacademy.kursproject.workshop.menu.menuoperation;

import by.itacademy.kursproject.workshop.operations.PercentOfMaterialCosts;
import by.itacademy.kursproject.workshop.service.ServiceList;

import java.util.ResourceBundle;

public class MenuPercentForEachMaterialCost implements MenuOperation {


    @Override
    public void execute(ServiceList serviceList) {
        System.out.println(new PercentOfMaterialCosts().execute(serviceList));
    }

    @Override
    public String name() {
        return ResourceBundle.getBundle("data").getString("MenuPercentForEachMaterialCost");
    }
}
