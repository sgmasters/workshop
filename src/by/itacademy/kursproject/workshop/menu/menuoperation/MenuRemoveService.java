package by.itacademy.kursproject.workshop.menu.menuoperation;

import by.itacademy.kursproject.workshop.operations.RemoveService;
import by.itacademy.kursproject.workshop.service.ServiceList;

import java.util.ResourceBundle;

public class MenuRemoveService extends ManageService {

    private int id;

    public void execute(ServiceList serviceList) {
        id = serviceList.getServices().size() + 1;
        new RemoveService(getServiceData()).execute(serviceList);
    }

    @Override
    public String name() {
        return ResourceBundle.getBundle("data").getString("MenuRemoveService");
    }

    @Override
    public int getId() {
        return id;
    }
}
