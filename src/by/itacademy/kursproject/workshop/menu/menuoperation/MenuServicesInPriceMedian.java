package by.itacademy.kursproject.workshop.menu.menuoperation;

import by.itacademy.kursproject.workshop.operations.ServicesInPriceMedian;
import by.itacademy.kursproject.workshop.service.ServiceList;

import java.util.ResourceBundle;

public class MenuServicesInPriceMedian implements MenuOperation {
    @Override
    public void execute(ServiceList serviceList) {
        System.out.println(new ServicesInPriceMedian().execute(serviceList));
    }

    @Override
    public String name() {
        return ResourceBundle.getBundle("data").getString("MenuServicesInPriceMedian");
    }
}
