package by.itacademy.kursproject.workshop.menu.menuoperation;

import by.itacademy.kursproject.workshop.service.Complexity;
import by.itacademy.kursproject.workshop.service.MaterialCosts;
import by.itacademy.kursproject.workshop.service.Service;

import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;

public abstract class ManageService implements MenuOperation {

    private static final Scanner SCANNER = new Scanner(System.in);
    private List<Complexity> complexities = Arrays.asList(Complexity.values());
    private List<MaterialCosts> materialCosts = Arrays.asList(MaterialCosts.values());

    protected Service getServiceData() {
        ResourceBundle bundle = ResourceBundle.getBundle("data");
        System.out.println(bundle.getString("inputServiceData") + "\n" + bundle.getString("name") + ":");
        String name = SCANNER.nextLine();
        System.out.println(bundle.getString("runtime") + ":");
        double runTime = Integer.parseInt(SCANNER.nextLine());
        System.out.println(bundle.getString("price") + ":");
        int price = Integer.parseInt(SCANNER.nextLine());
        chooseParameter(complexities, bundle.getString("complexity"));
        Complexity complexity = complexities.get(Integer.parseInt(SCANNER.nextLine()) - 1);
        chooseParameter(materialCosts, bundle.getString("materialCosts"));
        MaterialCosts materialCost = materialCosts.get(Integer.parseInt(SCANNER.nextLine()) - 1);
        return new Service(name, runTime, price, complexity, materialCost, getId());
    }

    private void chooseParameter(List e, String name) {
        StringBuilder s = new StringBuilder(name + ":\n");
        for (int i = 0; i < e.size(); i++) {
            s.append(i + 1).append(" ").append(e.get(i)).append("\n");
        }
        System.out.println(s);
    }

    public abstract int getId();
}
