package by.itacademy.kursproject.workshop.menu.menuoperation;

import by.itacademy.kursproject.workshop.operations.SearchByName;
import by.itacademy.kursproject.workshop.service.ServiceList;

import java.util.ResourceBundle;

public class MenuSearchByName implements MenuOperation {

    @Override
    public void execute(ServiceList serviceList) throws NullPointerException {
        System.out.println(ResourceBundle.getBundle("data").getString("inputName") + ":");
        System.out.println(new SearchByName().execute(serviceList));

    }

    @Override
    public String name() {
        return ResourceBundle.getBundle("data").getString("SearchByName");
    }
}
