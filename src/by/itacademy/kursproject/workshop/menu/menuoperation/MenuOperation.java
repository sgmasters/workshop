package by.itacademy.kursproject.workshop.menu.menuoperation;

import by.itacademy.kursproject.workshop.service.ServiceList;

public interface MenuOperation {
    void execute(ServiceList serviceList);
    String name();
}
