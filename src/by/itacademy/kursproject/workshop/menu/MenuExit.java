package by.itacademy.kursproject.workshop.menu;

import by.itacademy.kursproject.workshop.service.ServiceList;

import java.util.ResourceBundle;

public class MenuExit implements Menu {

    @Override
    public void execute(ServiceList serviceList) {
        System.exit(0);
    }

    @Override
    public String name() {
        return ResourceBundle.getBundle("data").getString("exit");
    }
}
