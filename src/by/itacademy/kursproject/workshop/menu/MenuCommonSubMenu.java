package by.itacademy.kursproject.workshop.menu;

import by.itacademy.kursproject.workshop.menu.menuoperation.MenuOperation;
import by.itacademy.kursproject.workshop.menu.root.RootMenuItem;
import by.itacademy.kursproject.workshop.service.ServiceList;

import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class MenuCommonSubMenu implements Menu {
    private static final Scanner SCANNER = new Scanner(System.in);
    private MenuOperation[] subMenus;
    private RootMenuItem rootMenuItem;
    private static final Logger LOGGER = Logger.getLogger(MenuCommonSubMenu.class.getName());

    public MenuCommonSubMenu(MenuOperation[] subMenus, RootMenuItem rootMenuItem) {
        this.rootMenuItem = rootMenuItem;
        this.subMenus = subMenus;
    }

    public void execute(ServiceList serviceList) {
        ResourceBundle bundle = ResourceBundle.getBundle("data");
        StringBuilder s = new StringBuilder(bundle.getString("chooseMenuItem") + ":\n");
        for (int i = 0; i < subMenus.length; i++) {
            s.append(i + 1).append(" ").append(subMenus[i].name()).append("\n");
        }
        System.out.print(s);
        try {
            subMenus[Integer.parseInt(SCANNER.next()) - 1].execute(serviceList);
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.err.println("Incorrect Argument");
            LOGGER.log(Level.INFO, e.getMessage());
            this.execute(serviceList);
        } catch (NullPointerException e1) {
            LOGGER.log(Level.INFO, e1.getMessage());
            this.execute(serviceList);
        }
        rootMenuItem.execute();
    }
}
