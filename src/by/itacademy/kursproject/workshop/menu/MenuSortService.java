package by.itacademy.kursproject.workshop.menu;

import by.itacademy.kursproject.workshop.menu.menuoperation.MenuOperation;
import by.itacademy.kursproject.workshop.menu.root.RootMenuItem;
import by.itacademy.kursproject.workshop.operations.SortById;
import by.itacademy.kursproject.workshop.operations.SortByRunTimeAndComplexity;

import java.util.ResourceBundle;

public class MenuSortService extends MenuCommonSubMenu implements Menu {
    private static final MenuOperation[] subMenus = {
            new SortById(),
            new SortByRunTimeAndComplexity()
    };

    public MenuSortService(RootMenuItem rootMenuItem) {
        super(subMenus, rootMenuItem);
    }

    @Override
    public String name() {
        return ResourceBundle.getBundle("data").getString("sortServices");
    }
}
