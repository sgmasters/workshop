package by.itacademy.kursproject.workshop.comparators;

import by.itacademy.kursproject.workshop.service.Service;

import java.util.Comparator;

public class ById implements Comparator<Service> {

    @Override
    public int compare(Service o1, Service o2) {
        return o1.getId() - o2.getId();
    }
}
