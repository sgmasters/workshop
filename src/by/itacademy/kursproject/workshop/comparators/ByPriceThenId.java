package by.itacademy.kursproject.workshop.comparators;

import by.itacademy.kursproject.workshop.service.Service;

import java.util.Comparator;

public class ByPriceThenId implements Comparator<Service> {
    @Override
    public int compare(Service o1, Service o2) {
        int result = o1.getPrice() - o2.getPrice();
        return (result == 0) ? (o1.getId() - o2.getId()) : result;
    }
}
