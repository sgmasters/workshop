package by.itacademy.kursproject.workshop.comparators;

import by.itacademy.kursproject.workshop.service.Service;

import java.util.Comparator;

public class ByRunTimeAndComplexity implements Comparator<Service> {
    @Override
    public int compare(Service o1, Service o2) {
        int result = (int) (o1.getRunTime() - o2.getRunTime());
        return (result == 0) ? o2.getComplexity().name().compareTo((o1.getComplexity().name())) : result;
    }
}
