mkdir target
javac -d target -encoding UTF-8 -sourcepath src src\by\itacademy\kursproject\workshop\Main.java
java -Djava.util.logging.config.file=logging.properties -cp target by.itacademy.kursproject.workshop.Main
pause